package sheridan;
/*
*@author Andy Tran - 991551797
*Midterm
*/
public class Celsius {

	public static int fromFahrenheit(int c) {
		return (int) Math.round(((double)c - 32) * 5 / 9);
	}
}
