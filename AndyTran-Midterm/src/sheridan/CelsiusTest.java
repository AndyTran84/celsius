package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * 
 * @author Andy Tran - 991551797
 * Software Process Management Midterm
 */

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		assertTrue("Invalid Case", Celsius.fromFahrenheit(68) == 20);
	}
	
	@Test
	public void testFromFahrenheitException() {
		assertFalse("Invalid Case", Celsius.fromFahrenheit(68) == 15);
	}
	
	@Test
	public void testFromFahrenheitBIN() {
		assertTrue("Invalid Case", Celsius.fromFahrenheit(-40) == -40);
	}
	
	@Test
	public void testFromFahrenheitBOUT() {
		//result is -40.55 C
		assertFalse("Invalid Case", Celsius.fromFahrenheit(-41) == -40);
	}

}
